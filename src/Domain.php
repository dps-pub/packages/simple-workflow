<?php
namespace DPS\Aws\Swf;

use DPS\Aws\Swf\Client\SwfClient;
use Symfony\Component\Console\Output\OutputInterface;

abstract class Domain
{
    /**
     * @var SwfClient
     */
    public static $client;

    /**
     * @var Workflow[]
     */
    protected $workflows = [];

    public function __construct()
    {
        $this->configure();
    }

    public function getName()
    {
        if (property_exists($this, 'name')) {
            return $this->name . '-' . app()->environment();
        }

        return get_class($this) . '-' . app()->environment();
    }

    /**
     * @param null $name
     * @return Domain
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param string $workflow
     */
    public function startWorkflow($workflowName)
    {
        $workflow = $this->workflows[$workflowName];
        $workflow->start();
    }

    public function pollForActivities(OutputInterface $output)
    {
        $this->register();
        echo "Starting to polling for activities\n";
        while (true) {
            echo "A: Polling...\n";
            $result = static::$client->pollForActivityTask(array(
                "domain" => $this->getName(),
                "taskList" => array(
                    "name" => "main"
                ),
            ));

            $this->processActivities($result);
        }
    }

    public function pollForDecisions(OutputInterface $output)
    {
        $this->register();
        echo "Starting to polling for decisions\n";
        while (true) {
            echo "D: Polling...\n";
            $result = static::$client->pollForDecisionTask([
                "domain" => $this->getName(),
                "taskList" => [
                    "name" => "main"
                ],
                "identify" => "default",
                "maximumPageSize" => 50,
                "reverseOrder" => true
            ]);

            $this->processDecisions($result);
        }
    }

    public function register() {
        $result = [];
        try {
            $result = Domain::$client->describeDomain([
                'name' => $this->getName(),
            ]);
        } catch (\Exception $e) {}

        if (isset($result['domainInfo']) && $result['domainInfo']['status'] == 'REGISTERED') {
            return;
        }


        Domain::$client->RegisterDomain([
            'workflowExecutionRetentionPeriodInDays' => (string) $this->getRetention(),
            'name' => $this->getName(),
        ]);


        collect($this->workflows)->each->register();
    }

    abstract public function configure();

    /**
     * @param Workflow|string $workflow
     */
    protected function add($workflow)
    {
        if (is_string($workflow)) {
            $workflow = new $workflow;
        }

        $this->workflows[$workflow->getName()] = $workflow;

        $workflow->setDomain($this);
    }

    private function processActivities($result)
    {
        if (!$result['taskToken']) return;
        $taskToken = $result['taskToken'];
        Domain::$client->recordActivityTaskHeartbeat([
            'taskToken' => $taskToken
        ]);

        $activity = unserialize($result['input']);
//        dump($class);
        $context = new ActivityContext($result);
        try {
            $response = call_user_func([$activity, 'handle'], $context);
            try {
                Domain::$client->respondActivityTaskCompleted(array(
                    "taskToken" => $taskToken,
                    "result" => $response
                ));
            } catch (\Exception $e) {
                dump('Error marking activity as completed', $e->getMessage(), get_class($e));
            }
        } catch (\Exception $e) {

            dump($e->getMessage());
            dump($e->getTrace());

            Domain::$client->respondActivityTaskFailed([
                "taskToken" => $taskToken,
                "reason" => substr($e->getMessage(), 0, 255),
                "details" => $e->getTraceAsString()
            ]);
        }
    }

    private function processDecisions($result)
    {
        if (!isset($result['events'])) return;

        $workflowName = $result['workflowType']['name'];

        if (!isset($this->workflows[$workflowName])) {
            echo "unknown workflow $workflowName\n";
            return;
        }

        $this->workflows[$workflowName]->handle($result);
    }

    public function getRetention()
    {
        return 10;
    }

    public function getWorkflow($name)
    {
        return $this->workflows[$name];
    }
}
