<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 26/6/17
 * Time: 15:48
 */

namespace DPS\Aws\Swf;


use App\Workflows\ApplicationDomain;

abstract class Activity
{
    const ACTIVITY_TASK_SCHEDULED = 'ActivityTaskScheduled';
    const ACTIVITY_TASK_STARTED = 'ActivityTaskStarted';
    const ACTIVITY_TASK_COMPLETED = 'ActivityTaskCompleted';
    const ACTIVITY_TASK_FAILED = 'ActivityTaskFailed';

    protected $version;
    /** @var  Workflow */
    protected $workflow;
    protected $workflowName;
    protected $domainName;

    public abstract function handle();
    public abstract function getId();

    public function getControlMessage() {
        return "";
    }

    public function timeout() {
        return 5;
    }

    public function register() {
        $result = [];
        try {
            $result = Domain::$client->describeActivityType([
                'activityType' => [
                    'name' => $this->name,
                    'version' => (string)$this->version,
                ],
                'domain' => $this->workflow->getDomain()->getName(),
            ]);
        } catch (\Exception $e) {}

        if (isset($result['typeInfo']) && $result['typeInfo']['status'] == 'REGISTERED') {
            return;
        }

        Domain::$client->registerActivityType([
            'defaultTaskList' => [
                'name' => 'main',
            ],
            'domain' => $this->workflow->getDomain()->getName(),
            'name' => $this->name,
            'version' => (string) $this->version,
        ]);
    }

    public function getName()
    {
        if (property_exists($this, 'name')) {
            return $this->name;
        }

        return get_class($this);
    }

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param mixed $version
     * @return $this
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    public function setWorkflow(Workflow $workflow)
    {
        $this->workflow = $workflow;
    }

    public function __sleep()
    {
        $this->workflowName = $this->workflow->getName();
        $this->domainName = $this->workflow->getDomain()->getName();
        return ['workflowName', 'domainName'];
    }

    public function __wakeup(){
        $domain = \SimpleWorkflow::getDomain($this->domainName);
        $this->workflow = $domain->getWorkflow($this->workflowName);
    }

    public function toScheduleActivityTask()
    {

        $timeout = "NONE";
        if ($this->event->timeout()) {
            $timeout = (string) ($this->event->timeout() * 60);
        }

        return [
            "decisionType" => "ScheduleActivityTask",
            "scheduleActivityTaskDecisionAttributes" => [
                "activityType" => [
                    "name" => $this->getName(),
                    "version" => $this->getVersion(),
                ],
                "activityId" => $this->getId(),
                "control" => $this->getControlMessage(),
                "scheduleToCloseTimeout" => $timeout,
                "scheduleToStartTimeout" => $timeout,
                "startToCloseTimeout" => $timeout,
                "heartbeatTimeout" => $timeout,
                "taskList" => [
                    "name" => "main",
                ],
                "input" => serialize($this),
            ]
        ];
    }
}
