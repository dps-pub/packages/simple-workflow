<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 26/6/17
 * Time: 15:43
 */

namespace DPS\Aws\Swf\Laravel;

use Aws\Swf\SwfClient;
use DPS\Aws\Swf\Domain;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    protected $defer = true;

    public function register()
    {
        $this->app->singleton('dps.swf.container', function ($app) {
            $client =  new SwfClient([
                'version' => 'latest',
                'region' => 'ap-southeast-2',
            ]);

            $workflow = $app->make(SimpleWorkflow::class);
            SimpleWorkflow::$client = $client;
            return $workflow;
        });
    }

    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                Commands\ActivityWorker::class,
                Commands\DecisionWorker::class,
                Commands\StartWorkflow::class,
            ]);

        }
    }

    public function provides()
    {
        return [
            'dps.swf.container'
        ];
    }
}