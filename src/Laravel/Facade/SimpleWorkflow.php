<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 30/6/17
 * Time: 10:30
 */

namespace DPS\Aws\Swf\Laravel\Facade;


use Illuminate\Support\Facades\Facade;

class SimpleWorkflow extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'dps.swf.container';
    }
}