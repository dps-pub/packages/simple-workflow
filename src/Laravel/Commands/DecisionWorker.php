<?php
namespace DPS\Aws\Swf\Laravel\Commands;


use DPS\Aws\Swf\Laravel\Facade\SimpleWorkflow;
use Illuminate\Console\Command;

class DecisionWorker extends Command
{
    protected $signature = "swf:decision-worker {domain}";

    public function handle()
    {
        $domain = SimpleWorkflow::getDomain($this->argument('domain') . '-' . app()->environment());
        $domain->pollForDecisions($this->output);
    }
}
