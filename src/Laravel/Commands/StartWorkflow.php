<?php
namespace DPS\Aws\Swf\Laravel\Commands;

use DPS\Aws\Swf\Domain;
use DPS\Aws\Swf\Laravel\Facade\SimpleWorkflow;
use Illuminate\Console\Command;

class StartWorkflow extends Command
{
    protected $signature = "swf:start-workflow {domain} {workflow} {--d|demonize} {--s|sleep=30}";

    public function handle()
    {
        $domain = SimpleWorkflow::getDomain($this->argument('domain') . '-' . app()->environment());
        $domain->register();
        if ($this->option('demonize')) {
            $this->output->writeln("starting in demonize mode");
            while (true) {
                try {
                    $this->loop($domain);
                } catch (\Exception $e) {
                    echo $e->getMessage()."\n";
                    echo $e->getTraceAsString()."\n";
                }
                sleep($this->option('sleep'));
            }
        } else {
            $this->loop($domain);
        }
    }

    public function loop($domain)
    {
        /** @var Domain $domain */
        dump('starting workflow');
        $domain->startWorkflow($this->argument('workflow'));
    }
}
