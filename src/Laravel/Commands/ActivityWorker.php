<?php
namespace DPS\Aws\Swf\Laravel\Commands;

use DPS\Aws\Swf\Domain;
use DPS\Aws\Swf\Laravel\Facade\SimpleWorkflow;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ActivityWorker extends Command
{
    protected $signature = "swf:activity-worker {domain}";

    public function handle()
    {
        $domain = $this->argument('domain') . '-' . app()->environment();;
        /** @var Domain $domain */
        $domain = app('dps.swf.container')->getDomain($domain);
        $domain->pollForActivities($this->output);
    }
}
