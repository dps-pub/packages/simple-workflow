<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 30/6/17
 * Time: 10:26
 */

namespace DPS\Aws\Swf\Laravel;


use DPS\Aws\Swf\Domain;

class SimpleWorkflow
{
    public static $client;
    /** @var Domain[] */
    protected $domains = [];

    public function addDomain($domain)
    {
        if (is_string($domain)) {
            $domain = app($domain);
        }

        $domain::$client = static::$client;

        $this->domains[$domain->getName()] = $domain;
    }

    public function getDomain($name)
    {
        return $this->domains[$name];
    }

    public function getDomains()
    {
        return $this->domains;
    }
}
