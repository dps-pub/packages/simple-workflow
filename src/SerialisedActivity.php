<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 26/6/17
 * Time: 16:37
 */

namespace DPS\Aws\Swf;

abstract class SerialisedActivity extends Activity
{
    protected $name = 'SerialisedActivity';
    protected $version = '1.0';

    public $callable;
    public $arguments;

    public function __construct($callable, array $arguments = [])
    {
        $this->callable = $callable;
        $this->arguments = $arguments;
    }

    public function handle()
    {
        call_user_func($this->callable, \Illuminate\Foundation\Application::getInstance());
    }

    public function __sleep()
    {
        $parent = parent::__sleep();
        return array_merge($parent, ['callable', 'arguments']);
    }
}
