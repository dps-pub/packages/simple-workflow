<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 26/6/17
 * Time: 15:47
 */

namespace DPS\Aws\Swf;

use \SuperClosure\Serializer;

class Decision
{
    const DECISION_TASK_SCHEDULED = 'DecisionTaskScheduled';
    const DECISION_TASK_STARTED = 'DecisionTaskStarted';
    const DECISION_TASK_COMPLETED = 'DecisionTaskCompleted';
    /**
     * @var Workflow
     */
    protected $workflow;
    protected $result;
    protected $decisions = [];

    public function __construct(Workflow $workflow, $result)
    {
        $this->workflow = $workflow;
        $this->result = $result;
    }

    public function runActivity(Activity $activity)
    {

        $activity->setWorkflow($this->workflow);
        $activity->register();

        $this->decisions[] = $activity->toScheduleActivityTask();
    }

    public function respondComplete($finishIfEmpty = true)
    {
        if (count($this->decisions) === 0 && $finishIfEmpty) {
            $this->workflowFinished();
        }

        dump($this->decisions);

        Domain::$client->respondDecisionTaskCompleted([
            "taskToken" => $this->result['taskToken'],
            "decisions" => $this->decisions
        ]);
    }

    public function workflowFinished()
    {
        $this->decisions[] = $this->completeWorkflowExecution();
    }

    private function completeWorkflowExecution()
    {
        return [
            'decisionType' => Workflow::COMPLETE_WORKFLOW_EXECUTION,
        ];
    }
}
