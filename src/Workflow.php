<?php
namespace DPS\Aws\Swf;

use Carbon\Carbon;

abstract class Workflow
{
    const WORKFLOW_EXECUTION_STARTED = 'WorkflowExecutionStarted';
    const WORKFLOW_EXECUTION_TIMED_OUT = 'WorkflowExecutionTimedOut';
    const COMPLETE_WORKFLOW_EXECUTION = 'CompleteWorkflowExecution';

    protected $deciders = [];
    /** @var Domain */
    protected $domain;

    public function __construct()
    {
        $this->configure();
    }

    abstract public function configure();
    abstract public function getVersion();

    public function getName()
    {
        if (property_exists($this, 'name')) {
            return $this->name;
        }

        return get_class($this);
    }

    public function setDomain(Domain $domain)
    {
        $this->domain = $domain;
    }

    public function register() {
        $result = [];
        try {
            $result = Domain::$client->describeWorkflowType([
                'domain' => $this->domain->getName(),
                'workflowType' => [
                    'name' => $this->getName(),
                    'version' => (string) $this->getVersion(),
                ],
            ]);
        } catch (\Exception $e) {}

        if (isset($result['typeInfo']) && $result['typeInfo']['status'] == 'REGISTERED') {
            return;
        }

        Domain::$client->registerWorkflowType([
            'defaultTaskList' => [
                'name' => 'main',
            ],
            'domain' => $this->getDomain()->getName(),
            'name' => $this->getName(),
            'version' => (string) $this->getVersion(),
        ]);
    }

    public function handle($result)
    {
        $events = collect($result['events']);
        $decisionIndex = $events->filter(function ($event) {
            return $event['eventType'] === Decision::DECISION_TASK_STARTED;
        })->count() - 1;
        if ($this->hasDecider($decisionIndex)) {
            return $this->runDecider($decisionIndex, $result);
        }

        dump('Finishing workflow ' . $this->getName() . ": " . $result['workflowId']);

        $scheduled = $events->filter(function ($event) {
            return $event['eventType'] === Activity::ACTIVITY_TASK_SCHEDULED;
        })->count();
        $finished = $events->filter(function ($event) {
            return $event['eventType'] === Activity::ACTIVITY_TASK_COMPLETED ||
                   $event['eventType'] === Activity::ACTIVITY_TASK_FAILED;
        })->count();
        $shouldFinish = $finished === $scheduled;

        $decision = $this->getDecision($result);
        $decision->respondComplete($shouldFinish);
    }

    protected function hasDecider($index)
    {
        return isset($this->deciders[$index]);
    }

    public function start()
    {
        $id = $this->newId();

        $options = [
            'domain' => $this->getDomain()->getName(),
            'executionFilter' => [
                'workflowId' => "$id"
            ],
            'startTimeFilter' => [
                'oldestDate' => Carbon::now()->subMinutes(5)->timestamp
            ]
        ];
        $result = Domain::$client->listOpenWorkflowExecutions($options);
        if (count($result['executionInfos']) > 0) {
            dump($result['executionInfos']);
            dump("workflow for $id already running");
            return;
        }
        $result = Domain::$client->listClosedWorkflowExecutions($options);
        if (count($result['executionInfos']) > 0) {
            dump("workflow for $id already finished");
            return;
        }

        Domain::$client->startWorkflowExecution([
            "domain" => $this->domain->getName(),
            "workflowId" => "$id",
            "workflowType" => array(
                "name" => $this->getName(),
                "version" => (string) $this->getVersion()
            ),
            "taskList" => array(
                "name" => "main"
            ),
            "input" => "$id",
            "executionStartToCloseTimeout" => "43200", // Magic 12 Hour number
            'taskStartToCloseTimeout' => "43200", // Magic 12 Hour number
            "childPolicy" => "TERMINATE",
        ]);
    }

    public function getDomain()
    {
        return $this->domain;
    }

    protected function decider($method)
    {
        $this->deciders[] = $method;

        return $this;
    }

    protected function thenFinish()
    {
        $this->deciders[] = 'finishWorkflow';
    }

    public function finishWorkflow(Decision $decision)
    {
        $decision->workflowFinished();
    }

    abstract public function newId();

    private function runDecider($int, $result)
    {
        $decider = $this->deciders[$int];

        $decision = $this->getDecision($result);

        dump("running $decider in " . get_class($this));

        call_user_func([$this, $decider], $decision);

        $decision->respondComplete();
    }

    /**
     * @param $result
     * @return Decision
     */
    private function getDecision($result): Decision
    {
        $decision = new Decision($this, $result);

        return $decision;
    }
}
