<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 23/6/17
 * Time: 18:16
 */

namespace DPS\Aws\Swf\Client;


use Aws\Result;

class ReplayClient
{
    function __call($name, $arguments)
    {
        $path = __DIR__ . DIRECTORY_SEPARATOR . 'requests.json';
        $data = [];
        if (file_exists($path)) {
            $data = json_decode(file_get_contents($path), true);
        }

        foreach ($data as $datum) {
            if ($datum['request']['name'] === $name) {
                if ($datum['request']['arguments'] === $arguments) {
                    return new Result($datum['response']);
                }
            }
        }
    }
}