<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 23/6/17
 * Time: 16:10
 */

namespace DPS\Aws\Swf\Client;

/**
 * Class SwfClient
 * @package DPS\Aws\Swf\Client
 * @mixin \Aws\Swf\SwfClient
 */
class SwfClient
{
    /**
     * @var \Aws\Swf\SwfClient
     */
    protected $client;

    public function __construct(\Aws\Swf\SwfClient $client)
    {
        $this->client = $client;
    }

    public function __call($name, $arguments)
    {
        $request = compact('name', 'arguments');
        /** @var \Aws\Result $response */
        $return = call_user_func_array([$this->client, $name], $arguments);
        $response = $return->toArray();

        $path = __DIR__ . DIRECTORY_SEPARATOR . 'requests.json';
        $data = [];
        if (file_exists($path)) {
            $data = json_decode(file_get_contents($path), true);
        }

        $data[] = compact('request', 'response');
        file_put_contents($path, json_encode($data, JSON_PRETTY_PRINT));

        return $return;
    }
}