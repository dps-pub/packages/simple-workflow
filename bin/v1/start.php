<?php
require __DIR__ . '/../../vendor/autoload.php';

/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 26/6/17
 * Time: 13:29
 */

use Aws\Swf\SwfClient;

// Create an instance of the SWF class
$client = new SwfClient([
    'version' => 'latest',
    'region' => 'ap-southeast-2',
]);
while (true) {

    $workflowId = (new \Carbon\Carbon)->second(0)->timestamp;
    echo "Starting new workflow for ".  \Carbon\Carbon::createFromTimestamp($workflowId)->toDateTimeString() . " \n";
    // Starts a new instance of our workflow
    try {
        $client->startWorkflowExecution(array(
            "domain" => "testing",
            "workflowId" => "$workflowId",
            "workflowType" => array(
                "name" => "cron",
                "version" => "1"
            ),
            "taskList" => array(
                "name" => "main"
            ),
            "input" => "$workflowId",
            "executionStartToCloseTimeout" => "300",
            'taskStartToCloseTimeout' => "300",
            "childPolicy" => "TERMINATE"
        ));
        echo "Started workflow\n";
    } catch (Exception $e) {
        echo "Workflow already started \n";
    }

    sleep(30);
}