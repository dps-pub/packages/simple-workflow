<?php
require __DIR__ . '/../../vendor/autoload.php';

/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 26/6/17
 * Time: 13:29
 */

use Aws\Swf\SwfClient;

function getMemory() {
    $size = memory_get_usage(true);
    $unit=array('b','kb','mb','gb','tb','pb');
    return round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
}

function _step(SwfClient $client) {
    echo "Polling... " . getMemory() . "\n";
    $result = $client->pollForActivityTask(array(
        "domain" => "testing",
        "taskList" => array(
            "name" => "main"
        ),
    ));

    processActivity($result, $client);
}

function poll() {
    // Create an instance of the SWF class
    $client = new SwfClient([
        'version' => 'latest',
        'region' => 'ap-southeast-2',
    ]);
    while (true) {
        _step($client);
    }
}

function processActivity($activity, SwfClient $client) {
    if (isset($activity['taskToken'])) {
        dump($activity);
        $client->respondActivityTaskCompleted(array(
            "taskToken" => $activity['taskToken'],
            "result" => "I've finished!"
        ));
    } else {
        echo "No Events \n";
    }
}

function processEvent($event) {
    var_dump($event);
}

poll();