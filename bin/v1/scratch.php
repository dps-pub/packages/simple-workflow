<?php
namespace DPS\Aws\Swf;
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 26/6/17
 * Time: 13:26
 */

/**
 * New ID for cron workflows
 *
 * @return int
 */
function new_id() {
    return (new \Carbon\Carbon)->second(0)->timestamp;
}
