<?php
require __DIR__ . '/../../vendor/autoload.php';
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 26/6/17
 * Time: 13:29
 */


use Aws\Swf\SwfClient;

function getMemory() {
    $size = memory_get_usage(true);
    $unit=array('b','kb','mb','gb','tb','pb');
    return round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
}

function _step($client) {
    echo "Polling... " . getMemory() . "\n";
    $result = $client->pollForDecisionTask(array(
        "domain" => "testing",
        "taskList" => array(
            "name" => "main"
        ),
        "identify" => "default",
        "maximumPageSize" => 50,
        "reverseOrder" => true
    ));

    processEvents($result, $client);
}

function poll() {

    // Create an instance of the SWF class
    $client = new SwfClient([
        'version' => 'latest',
        'region' => 'ap-southeast-2',
    ]);
    while (true) {
        _step($client);
    }
}

function processEvents($payload, SwfClient $client) {
    if (isset($payload['events'])) {
        $d = new \Illuminate\Support\Debug\Dumper;
        $d->dump($payload);

        $task_token = $payload["taskToken"];
        $workflow_id = $payload["workflowExecution"]["workflowId"];
        $run_id = $payload["workflowExecution"]["runId"];
        $last_event = $payload["events"][0]["eventId"];

        if($last_event == "3") {
            $client->respondDecisionTaskCompleted([
                "taskToken" => $task_token,
                "decisions" => [[
                    "decisionType" => "ScheduleActivityTask",
                    "scheduleActivityTaskDecisionAttributes" => [
                        "activityType" => array(
                            "name" => "activityOne",
                            "version" => "1.0"
                        ),
                        "activityId" => "1",
                        "control" => "this is a sample message",
                        // Customize timeout values
                        "scheduleToCloseTimeout" => "360",
                        "scheduleToStartTimeout" => "300",
                        "startToCloseTimeout" => "60",
                        "heartbeatTimeout" => "60",
                        "taskList" => array(
                            "name" => 'main'
                        ),
                        "input" => "this is a sample message"
                    ]
                ]],
            ]);
        } else if($last_event == "9") {
            $client->respondDecisionTaskCompleted([
                "taskToken" => $task_token,
                "decisions" => [[
                    "decisionType" => "CompleteWorkflowExecution",
                ]]
            ]);
        }

    } else {
        echo "No Events \n";
    }
}

poll();




