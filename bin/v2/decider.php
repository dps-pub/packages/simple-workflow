<?php
use DPS\Aws\Swf\Example\ApplicationDomain;

require __DIR__ . '/../../vendor/autoload.php';

$client = require __DIR__.'/client.php';

$app = app(\Illuminate\Foundation\Application::class);
\Illuminate\Container\Container::setInstance($app);
app()->bind(Illuminate\Contracts\Container\Container::class, $app);

ApplicationDomain::$client = $client;

(new ApplicationDomain)->pollForDecisions();