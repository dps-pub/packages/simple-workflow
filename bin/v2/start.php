<?php
use DPS\Aws\Swf\Example\App\Workflow\ApplicationDomain;

define('LARAVEL_START', microtime(true));
require __DIR__ . '/../../vendor/autoload.php';

$app = new Illuminate\Foundation\Application(
    realpath(__DIR__ . '/app/')
);

$client = require __DIR__.'/client.php';

ApplicationDomain::$client = $client;
$domain = new ApplicationDomain;
while (true) {
    dump("starting for " . \Carbon\Carbon::now()->second(0));
    try {
        $domain->startWorkflow('cron');
    } catch (Exception $e) {
        dump("can't");
    }
    sleep(30);
}
