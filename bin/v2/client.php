<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 27/6/17
 * Time: 16:12
 */

use Aws\Swf\SwfClient;

return new SwfClient([
    'version' => 'latest',
    'region' => 'ap-southeast-2',
]);