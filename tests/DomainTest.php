<?php

use DPS\Aws\Swf\Client\SwfClient;
use DPS\Aws\Swf\Domain;
use DPS\Aws\Swf\Laravel\ServiceProvider;
use Orchestra\Testbench\TestCase;

class DomainTest extends TestCase
{
    protected function getPackageProviders($app)
    {
        return [ServiceProvider::class];
    }

    public function testRegisteringStuff()
    {
        $this->assertTrue(true);

        $swfClient = new \DPS\Aws\Swf\Client\ReplayClient();
        $domain = new \DPS\Aws\Swf\Example\ApplicationDomain();
        $domain->setClient($swfClient);

    }

    public function testRunningAnExecution()
    {
        $this->assertTrue(true);
    }
}