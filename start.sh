#!/usr/bin/env sh
processes=()

function start {
    php artisan "swf:$1-worker" | sed "s/^/$1: /" &
    pid=$!
    processes+=($pid)
}

function stopAll {
    kill ${processes[*]}
}

start activity
start decision