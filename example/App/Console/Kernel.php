<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 30/6/17
 * Time: 09:30
 */

namespace DPS\Aws\Swf\Example\App\Console;


use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{

    protected function schedule(Schedule $schedule)
    {
        $schedule->command('inspire')
            ->everyMinute();
    }
}