<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 30/6/17
 * Time: 09:37
 */

namespace DPS\Aws\Swf\Example\App;


use Exception;
use Illuminate\Foundation\Exceptions\Handler;

class ExceptionHandler extends Handler
{
    public function renderForConsole($output, Exception $e)
    {
        throw $e;
    }
}