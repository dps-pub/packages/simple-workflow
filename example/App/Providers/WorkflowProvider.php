<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 30/6/17
 * Time: 10:54
 */

namespace DPS\Aws\Swf\Example\App\Providers;


use Aws\Swf\SwfClient;
use DPS\Aws\Swf\Domain;
use DPS\Aws\Swf\Example\App\Workflow\ApplicationDomain;
use DPS\Aws\Swf\Laravel\Facade\SimpleWorkflow;
use Illuminate\Support\ServiceProvider;

class WorkflowProvider extends ServiceProvider
{
    public function boot()
    {

        Domain::$client = new SwfClient([
            'region' => 'ap-southeast-2',
            'version' => 'latest'
        ]);

        SimpleWorkflow::addDomain(ApplicationDomain::class);
    }
}