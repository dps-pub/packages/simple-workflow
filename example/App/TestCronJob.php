<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 26/6/17
 * Time: 16:43
 */

namespace DPS\Aws\Swf\Example\App;


use DPS\Aws\Swf\Activity;

class TestCronJob
{
    public function handle(Activity $activity) {
        dump("well that worked");
    }
}