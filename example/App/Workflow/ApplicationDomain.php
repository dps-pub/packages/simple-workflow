<?php
namespace DPS\Aws\Swf\Example\App\Workflow;

use DPS\Aws\Swf\Domain;

class ApplicationDomain extends Domain
{
    public $name = 'AgedCareGuide';

    public function configure() {
        $this->add(CronWorkflow::class);
    }
}