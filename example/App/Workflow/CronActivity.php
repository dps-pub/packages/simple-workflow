<?php
namespace DPS\Aws\Swf\Example\App\Workflow;


use DPS\Aws\Swf\SerialisedActivity;
use Illuminate\Console\Scheduling\Event;

class CronActivity extends SerialisedActivity
{

    /**
     * @var Event
     */
    protected $event;

    public function __construct(Event $event)
    {
        $this->event = $event;

        parent::__construct([$this->event, 'run']);
    }

    public function getId()
    {
        return $this->event->mutexName();
    }
}