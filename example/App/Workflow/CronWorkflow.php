<?php
namespace DPS\Aws\Swf\Example\App\Workflow;

use DPS\Aws\Swf\Decision;
use DPS\Aws\Swf\Workflow;
use Illuminate\Cache\FileStore;
use Illuminate\Cache\Repository;
use Illuminate\Console\Scheduling\CacheMutex;
use Illuminate\Console\Scheduling\Event;
use Illuminate\Filesystem\Filesystem;

class CronWorkflow extends Workflow
{
    protected $name = 'cron';

    public function configure() {
        $this->decider('test')
            ->thenFinish();
    }

    public function test(Decision $decision) {
        $filesystem = new Filesystem();
        $store = new FileStore($filesystem, __DIR__.'/.cache');
        $cache = new Repository($store);
        $mutex = new CacheMutex($cache);
        $event = new Event($mutex, 'inspire');

        $decision->runActivity(new CronActivity($event));
    }

    public function newId()
    {
        return (new \Carbon\Carbon)->second(0)->timestamp;
    }

    public function getVersion()
    {
        return '1.1';
    }
}