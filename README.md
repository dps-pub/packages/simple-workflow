# Laravel SWF Workflows

Use AWS SWF to handle workflows in Laravel and PHP

## Installation
Simply run
```bash
composer require dps/simple-workflow
```
Then add `DPS\Aws\Swf\Laravel\ServiceProvider::class` to your providers array in `config/app.php`

and `'SimpleWorkflow' => DPS\Aws\Swf\Laravel\Facade\SimpleWorkflow::class,` to the aliases array in the same file


## Usage
Run this to create a new provider
```bash
php artisan make:provider WordflowProvider
```
